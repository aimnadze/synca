module.exports = {
    debug_mode: true,
    listen: {
        port: 9081,
        host: '127.0.0.1',
    },
}
