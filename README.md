Synca
=====

A web server accept a URL, queues it up and calls it in a sequence.
This Node.js software usually accompanies a PHP website where
a computationally heavy action is to be made.

Motivation
----------

PHP code is natively synchronous. If you need to make things
happen asynchronously you have these two options:

* Save the action in a database and execute later using a cron job, or
* Open a subprocess and execute the action in background.

Both of these options have drawbacks:

* Cron jobs execute at most once in a minute and the
  actions may have delayed for a minute or more, and
* Opening a subprocess is heavy and there are no
  simple ways to wait for a subprocess to finish.

The solution is to pass the action to another server that will
queue it and execute one by one in sync.

Below are some common use cases:

* Rebuilding a search index after an edit,
* Updating a dereferenced column in a database after an edit,
* Making redundant copies of an uploaded file.

Scripts
-------

* `./restart.sh` - start/restart the server.
* `./stop.sh` - stop the server.
* `./clean.sh` - clean the server after an unexpected shutdown.
* `./rotate.sh` - clean old logs.

Configuration
-------------

`config.js` contains the configuration.

Usage in PHP
------------
```
$ch = curl_init('http://localhost:9081/?' . http_build_query([
    'url' => 'https://example.com/',
]));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_exec($ch);
```

Technology
----------

* Node.js
