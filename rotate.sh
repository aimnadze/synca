#!/bin/bash
cd `dirname $BASH_SOURCE`

for i in out err
do
    file=log/index.$i
    if [ -f $file ]
    then
        echo "INFO Rotating $file"
        cat $file > $file.1
        truncate --size 0 $file
    fi
done
