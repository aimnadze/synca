module.exports = enqueue => request => {
    enqueue(String(request.parsed_url.query.url))
    request.respond(true)
}
