module.exports = req => {

    const accept_encoding = req.headers['accept-encoding']
    if (accept_encoding === undefined) return false

    const encodings = accept_encoding.split(', ')
    return encodings.indexOf('gzip') !== -1

}
