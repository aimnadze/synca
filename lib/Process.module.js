const url = require('url')

module.exports = app => (log, item, done) => {

    function load () {
        app.HttpGet(location, response => {

            if (response === true) {
                log_info('End')
                done()
                return
            }

            reload('Invalid response', response)

        }, err => {
            reload('Request failed:', err)
        })
    }

    function log_info (...args) {
        log.info('Process', item.id, ...args)
    }

    function reload (...args) {
        log_info(...args)
        trial++
        if (trial === 3) {
            log_info('Giving up')
            done()
            return
        }
        log_info('Retrying')
        setTimeout(load, 2000)
    }

    log_info('Start')

    const location = app.ParseLocation(item.url)

    let trial = 0
    load()

}
