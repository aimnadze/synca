const http = require('http')

const object = {}
;[
    [400, 'BadRequest'],
    [403, 'PayloadTooLarge'],
    [404, 'NotFound'],
].forEach(([code, name]) => {
    const text = http.STATUS_CODES[code]
    object[name] = request => {
        request.res.statusCode = code
        request.respond({
            error: 'HTTP_STATUS',
            code, text,
        })
    }
})

module.exports = object
