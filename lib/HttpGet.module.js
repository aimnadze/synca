const http = require('http')
const https = require('https')

module.exports = app => (location, done, error, options) => {

    if (typeof error !== 'function') throw new Error
    if (options === undefined) options = {}

    const request_module = location.scheme === 'https' ? https : http

    const req = request_module.get({
        ...location,
        path: location.path,
        headers: options.headers,
    }, res => {
        app.ReadText(res, responseText => {

            abort_timeout()
            req.off('error', error)
            req.on('error', () => {})

            let response
            try {
                response = JSON.parse(responseText)
            } catch {
                error({ code: 'INVALID_JSON' })
                return
            }

            done(response)

        })
    })
    req.on('error', error)

    const abort_timeout = app.Timeout(() => {
        req.abort()
    }, options.timeout || 30 * 1000)

}
