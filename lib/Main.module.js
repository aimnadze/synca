const http = require('http')
const url = require('url')

module.exports = app => () => {

    const listen = app.config.listen

    const enqueue = app.Queue()

    const pages = Object.create(null)
    pages['/add'] = app.AddPage(enqueue)

    require('http').createServer((req, res) => {

        const parsed_url = url.parse(req.url, true)

        const page = (() => {
            const page = pages[parsed_url.pathname]
            if (page === undefined) return app.ErrorPage.NotFound
            return page
        })()

        const request = {
            parsed_url, req, res,
            respond (response) {
                app.EchoText(request, {
                    type: 'application/json',
                    content: JSON.stringify(response),
                })
            },
        }

        page(request)

    }).listen(listen.port, listen.host)

    app.Watch(['lib'])

    app.log.info('Started')
    app.log.info('Listening http://' + listen.host + ':' + listen.port + '/')

}
