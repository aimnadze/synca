const crypto = require('crypto')

module.exports = app => () => {

    function next () {
        app.Process(log, items.shift(), () => {

            if (items.length === 0) {
                busy = false
                log.info('Finish processing')
                return
            }

            next()

        })
    }

    const log = app.log.sublog('Queue')

    const items = []

    let busy = false

    return url => {

        if (items.some(item => item.url === url)) {
            log.info('Ignoring: ' + JSON.stringify(url))
            return
        }

        const id = crypto.randomBytes(4).toString('hex')
        log.info('Enqueue ' + id + ': ' + JSON.stringify(url))
        items.push({ id, url })
        if (busy) return
        log.info('Start processing')
        busy = true
        next()

    }

}
