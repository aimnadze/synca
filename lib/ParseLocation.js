const url = require('url')

module.exports = location => {

    const parsed_location = url.parse(location, true)
    const port = parsed_location.port
    const scheme = (() => {
        const protocol = parsed_location.protocol
        if (protocol === null) return 'http'
        return protocol.substr(0, protocol.length - 1)
    })()

    return {
        scheme,
        host: parsed_location.hostname,
        port: port === null ? undefined : parseInt(port, 10),
        path: parsed_location.path,
    }

}
